select
  *
from
  entite
where
  particulier_id in (
    select
      id
    from
      particulier
    where
      nom ilike '%lagedamont%'
      and prenom ilike '%caroline%'
  );

-- Marche à suivre pour le triplet (SIRET, nom, prenom)
-- 1) créer l'entreprise pour le SIRET donné - OU - passer par l'interface pour visualiser le SIRET, ce qui créera Entreprise et Entité, puis supprimer l'Entité
-- 2) dans l'entité E du particulier (nom, prenom), supprimer particulier_id et setter entreprise_id au siret
-- 3) changer l'entite_type de E en 'PM'
-- 4) créer un nouveau contact (fonction inconnue) entre l'entité E et le particulier (nom, prenom)
-- 5) concaténer particulier.description dans le champ entite.activite_autre
-- 6) concaténer entite.future_enseigne dans le champ entite.activite_autre
-- 7) supprimer le champ entite.future_enseigne
-- 8) penser à gérer le flag exogène si SIRET exogène créé par l'action "Ajouter un établissement exogène"
