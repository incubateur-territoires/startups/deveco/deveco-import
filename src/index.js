require('dotenv').config();
const { existsSync } = require('fs');

const { postData, getDevecoEmail, readDataFromSheet } = require('./lib/helpers');

if (!process.env.DEVECO_EMAIL) {
	console.error(
		'You must set DEVECO_EMAIL to the email of the deveco that will own the imported data'
	);
	return;
}

const sheetsConfigs = [
	{
		sheet: 'Etiquettes',
		endpoint: 'etablissements',
		rowToData: (row) => ({
			siret: `${row[0] || ''}`,
			activites: `${row[2] ?? ''}`
				.split(';')
				.map((s) => s.trim())
				.filter(Boolean),
			zones: `${row[3] ?? ''}`
				.split(';')
				.map((s) => s.trim())
				.filter(Boolean),
			mots: `${row[4] ?? ''}`
				.split(';')
				.map((s) => s.trim())
				.filter(Boolean),
			autre: `${row[5] ?? ''}`,
		}),
	},
	{
		sheet: 'Contacts',
		endpoint: 'etablissement-contacts',
		rowToData: (row) => ({
			siret: `${row[0] || ''}`,
			fonction: row[2] || '',
			nom: row[3] || '',
			prenom: row[4] || '',
			email: row[5] || '',
			telephone: row[6] || '',
			dateDeNaissance: row[7] || '',
		}),
	},
	{
		sheet: 'Echanges',
		endpoint: 'etablissement-echanges',
		rowToData: (row) => ({
			siret: `${row[0] || ''}`,
			auteur: row[2] || '',
			titre: row[3] || '',
			type: row[4] || '',
			date: row[5] || '',
			contenu: row[6] || '',
		}),
	},
	// {
	//   sheet: "Créateurs d'entreprise",
	//   entityType: 'etablissement-creations',
	//   rowToData: (row) => ({
	//     nom: row[0] || '',
	//     prenom: row[1] || '',
	//     email: row[2] || '',
	//     telephone: row[3] || '',
	//     futureEnseigne: row[4] || '',
	//     description: row[5] || '',
	//   }),
	// },
];

const importSheet = async (excelPath, email, sheetConfig) => {
	const { sheet, endpoint, rowToData } = sheetConfig;

	console.info(`Importing sheet ${sheet}...`);

	let rows = [];

	try {
		rows = await readDataFromSheet({ excelPath, sheet });
	} catch (e) {
		console.error(e.message);
		return;
	}

	if (!rows || rows.length === 0) {
		console.info(`No rows in sheet ${sheet}, skipping`);
		return;
	}

	const data = { email, data: rows.map((row) => rowToData(row)) };

	const response = await postData({ endpoint, data });

	return new Promise((resolve, reject) => {
		response.data.on('error', reject).on('end', resolve).pipe(process.stdout);
	});
};

const importSheets = async (filePath) => {
	console.info('Importing file:', filePath);

	if (!existsSync(filePath)) {
		console.info('Error: File does not exist');
		process.exit(1);
	}

	const email = getDevecoEmail();

	console.info(`Importing for DevEco: ${email}\n`);

	for (const sheetConfig of sheetsConfigs) {
		await importSheet(filePath, email, sheetConfig);

		console.info('\n');
	}
};

const filePath = process.argv[2];

if (!filePath) {
	console.info('usage: yarn start /path/to/file');

	process.exit(1);
}

importSheets(filePath)
	.then(() => {
		console.info('Import is finished');
		process.exit(0);
	})
	.catch((error) => {
		console.error(error);
		process.exit(1);
	});
