const axios = require('axios').default;
const readXlsxFile = require('read-excel-file/node');

const readDataFromSheet = async ({ excelPath, sheet }) => {
	let rows = [];
	try {
		rows = await readXlsxFile(excelPath, { sheet });
		rows.shift();
		rows.shift();
	} catch (e) {
		console.error(e.message);
	}

	return rows;
};

const getDevecoEmail = () => {
	const email = process.env.DEVECO_EMAIL;

	if (!email) {
		throw new Error(
			'You must set DEVECO_EMAIL to the email of the deveco that will own the imported data'
		);
	}

	return email;
};

const postData = ({ endpoint, data }) => {
	const url = `${process.env.SITE_URL}/api/import/${endpoint}/${process.env.IMPORT_TOKEN || ''}`;
	const options = {
		responseType: 'stream',
		headers: {
			'Content-Type': 'application/json;charset=UTF-8',
		},
	};
	return axios.post(url, data, options);
};

module.exports = { postData, getDevecoEmail, readDataFromSheet };
